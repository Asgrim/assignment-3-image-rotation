//
// Created by andre on 17.10.2022.
//

#ifndef LAB3_IMAGE_H
#define LAB3_IMAGE_H
#include <inttypes.h>
#include <malloc.h>
#include <stdint.h>
#include <stdio.h>
struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
struct  __attribute__((packed)) pixel { uint8_t b, g, r; };

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct image create_image(const uint64_t width, const uint64_t height);
void free_image(const struct image *image);
struct image rotate( struct image const *source );
void print_header(const struct bmp_header *header);
void print_pixel(const struct pixel *px);
void print_image(const struct image *image);
#endif //LAB3_IMAGE_H
