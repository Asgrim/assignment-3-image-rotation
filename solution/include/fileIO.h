//
// Created by andre on 17.10.2022.
//

#ifndef LAB3_FILEIO_H
#define LAB3_FILEIO_H
#include "image.h"
#include <stdbool.h>
bool open_file_wb(FILE** file, const char* file_name );
bool open_file(FILE** file, const char* file_name);
bool close_file(FILE* file);
/*  deserializer   */
enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
    /* коды других ошибок  */
};

/*  serializer   */
enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR,
    WRITE_INVALID_FILE,
    WRITE_INVALID_HEADER,
    WRITE_INVALID_PADDING
    /* коды других ошибок  */
};
bool read_header(FILE *in, struct bmp_header *header);
enum read_status from_bmp( FILE* const in, struct image* img );
enum write_status to_bmp( FILE* const out, struct image const* img );
size_t get_padding(const size_t width);
#endif //LAB3_FILEIO_H
