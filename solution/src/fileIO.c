//
// Created by andre on 17.10.2022.
//
#include "fileIO.h"
bool open_file_wb(FILE** file, const char* file_name ){
    *file = fopen(file_name, "wb");
    return !(*file == NULL);
}
bool open_file(FILE** file, const char* file_name){
    *file = fopen(file_name, "rb");
    return !(*file == NULL);
}

bool close_file(FILE *file) {
    return fclose(file) == 0;
}

size_t get_padding(const size_t width){
    if(width % 4 == 0){
        return 0;
    }
    return 4 - ((width * sizeof(struct pixel)) % 4);
}

bool read_header(FILE *in, struct bmp_header *header){
    return fread(header, sizeof(struct bmp_header), 1, in) == 1 ;
}
enum read_status from_bmp( FILE* const in, struct image* img ){
    struct bmp_header header = {0};
    if(!read_header(in, &header)){
        return READ_INVALID_HEADER;
    }
    *img = create_image(header.biWidth, header.biHeight);
    size_t padding = get_padding(header.biWidth);
    size_t data_ind = 0;
    for (int i = 0; i < header.biHeight; ++i) {
        for (int j = 0; j < header.biWidth; ++j) {
            if(!fread((img->data + data_ind), sizeof (struct pixel), 1, in)){
                free_image(img);
                return READ_INVALID_BITS;
            }
            data_ind++;
        }
        if(fseek(in, (long)padding, SEEK_CUR)){
            free_image(img);
            return READ_INVALID_SIGNATURE;
        }
    }
    return READ_OK;
}
static struct bmp_header get_header( struct image const *source ){
    struct bmp_header bmpHeader = {
            .bfType = 0x4d42,
            .bfileSize = source->width * source->height * sizeof(struct pixel) +
                    get_padding(source->width) * source->height +
                    sizeof(struct bmp_header),
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = 40,
            .biWidth = source->width,
            .biHeight = source->height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = (source->width + get_padding(source->width)) * source->height * sizeof(struct pixel),
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0,

    };
    return bmpHeader;
}
enum write_status to_bmp( FILE* out, struct image const* img ){
    if (out == NULL) {
        return WRITE_INVALID_FILE;
    }
    struct bmp_header header = get_header(img);
    if (!fwrite(&header,sizeof(struct bmp_header),1,out)){
        return  WRITE_INVALID_HEADER;
    }
    size_t st = 0;
    const uint8_t zero_byte = 0;
    const size_t padding = get_padding(img->width);
    for (size_t row_ind = 0;row_ind < img->height; row_ind++){
        st = fwrite(img->data + (row_ind * img->width),sizeof(struct pixel),img->width,out);
        if (st != img->width){
            return WRITE_ERROR;
        }
        for (size_t i = 0; i < padding;i++){
            if (!fwrite(&zero_byte, sizeof(uint8_t),1,out)){
                return WRITE_INVALID_PADDING;
            }
        }
    }
    return WRITE_OK;
}
