//
// Created by andre on 17.10.2022.
//
#include "image.h"
struct image create_image(const uint64_t width, const uint64_t height){
    struct image img = {.width = width, .height = height,
            .data = malloc(width * height * sizeof(struct pixel))
            };
    return img;
}
void free_image(const struct image *image){
    free(image->data);
}
struct image rotate( struct image const *source ){
    size_t image_size = source->height * source->width;
    struct image new_image = {.height = source->width, .width = source->height,
            .data = malloc(source->width * source->height * sizeof(struct pixel))
    };
    size_t start_index = image_size - source->width - 1; // address of first image pixel in image array
    size_t i = 0;
    for (size_t col_ind = 0; col_ind < source->width; col_ind++) {
        //need to refactor
        start_index ++;
        size_t row_offset = 0;
        while (row_offset <= start_index ){
            new_image.data[i] = source->data[start_index - row_offset];
            row_offset += source->width;
            if (i < (image_size - 1)){
                i++;
            }
        }
    }


    return new_image;
}
void print_header(const struct bmp_header *header){
    printf("bfType: %"PRIu16"\n",header->bfType);
    printf("file size: %"PRIu32"\n",header->bfileSize);
    printf("bfReserved: %"PRIu32"\n",header->bfReserved);
    printf("bOffBits: %"PRIu32"\n",header->bOffBits);
    printf("biSize %"PRIu32"\n",header->biSize);
    printf("width: %"PRIu32"\n",header->biWidth);
    printf("height: %"PRIu32"\n",header->biHeight);
    printf("biPlanes: %" PRIu16"\n",header->biPlanes);
    printf("biBitCount: %"PRIu16"\n",header->biBitCount);
    printf("biCompression: %"PRIu32"\n",header->biCompression);
    printf("biSizeImage: %"PRIu32"\n",header->biSizeImage);
}

void print_pixel(const struct pixel *px){
    printf("r: %"PRIu8" g: %"PRIu8" b: %"PRIu8"\n",px->r,px->g,px->b);
}

void print_image(const struct image *image){
    printf("width: %"PRIu64"\n",image->width);
    printf("height: %"PRIu64"\n",image->height);
    size_t i = 0;
    size_t image_size = image->width * image->height;
    while (i < image_size){
        print_pixel(&image->data[i]);
        i++;
    }
 }
