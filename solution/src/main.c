#include  "fileIO.h"
int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning
    if (argc != 3){
//        printf("ERROR: invalid arg format\n"
//               "program receives 2 arguments in this format:\n"
//               "image-transformer <source-image> <transformed-image>\n"
//                );
        fprintf(stderr,"ERROR: invalid arg format\n"
                       "program receives 2 arguments in this format:\n"
                       "image-transformer <source-image> <transformed-image>\n");
        return 1;
    }
    char* input_file_name = argv[1];
    char* out_file_name = argv[2];
    FILE* input_file = NULL;
    FILE* out_file = NULL;
    if (!open_file(&input_file,input_file_name)){
        fprintf(stderr,"ERROR: can't open file\n");
//        printf("ERROR: can't open file\n");
        return 1;
    }
    struct image source = {0};
    if (from_bmp(input_file,&source) != READ_OK){
        fprintf(stderr,"ERROR: can't read file\n");
//        printf("ERROR: can't read file\n");
        return 1;
    }
    struct image const new_img = rotate(&source);
    close_file(input_file);
    free_image(&source);
    if (!open_file_wb(&out_file,out_file_name)){
        fprintf(stderr,"ERROR: can't open create file\n");
//        printf("ERROR: can't open create file\n");
        free_image(&new_img);
        return 1;
    }

    if(to_bmp(out_file,&new_img) != WRITE_OK){
        fprintf(stderr,"ERROR: can't create new image\n");
//        printf("ERROR: can't create new image\n");
        free_image(&new_img);
        close_file(out_file);
        return 1;
    }
    free_image(&new_img);
    close_file(out_file);

}
